/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
// const colors = require('@nuxtjs/tailwindcss')

module.exports = {
	content: [
		'./src/components/**/*.{js,ts,vue}',
		'./src/layouts/**/*.{js,ts,vue}',
		'./src/pages/**/*.{js,ts,vue}',
	],
	theme: {
		screens: {
			sm: '640px',
			md: '768px',
			lg: '1024px',
			xl: '1280px',
			'2xl': '1536px',
		},
		colors: ({ colors }) => ({
			inherit: colors.inherit,
			current: colors.current,
			transparent: colors.transparent,
			white: colors.white,
			black: { ...colors.black, 500: '#1B1B1B' },
			blue: { ...colors.blue, 500: '#293894', 600: '#23328F' },
			gray: { ...colors.gray, 300: '#CFD1C7', 400: '#DFE0D9' },
		}),
		fontFamily: {
			akzidenzgrotesk: [
				'Akzidenzgrotesk',
				'Trebuchet MS',
				'Helvetica',
				'sans-serif',
				'"Apple Color Emoji"',
				'"Segoe UI Emoji"',
				'"Segoe UI Symbol"',
				'"Noto Color Emoji"',
			],
		},
		extend: {
			letterSpacing: {
				default: '-0.5px',
			},
			animation: {
				bounce_right: 'bounce_right 1s infinite;',
			},
			keyframes: {
				bounce_right: {
					'0%, 100%': {
						transform: 'translateX(-25%)',
						'animation-timing-function':
							'cubic-bezier(0.8, 0, 1, 1)',
					},
					'50%': {
						transform: 'translateX(0)',
						'animation-timing-function':
							'cubic-bezier(0, 0, 0.2, 1)',
					},
				},
			},
		},
	},
	variants: {},
	plugins: [],
}
